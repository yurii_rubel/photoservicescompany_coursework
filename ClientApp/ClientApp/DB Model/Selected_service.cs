//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ClientApp.DB_Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Selected_service
    {
        public int Selected_services_id { get; set; }
        public int Order_id { get; set; }
        public int Photo_service_id { get; set; }
        public decimal Price_of_service { get; set; }
        public string Quantity_service { get; set; }
    
        public virtual Order Order { get; set; }
        public virtual Photo_service Photo_services { get; set; }
    }
}
