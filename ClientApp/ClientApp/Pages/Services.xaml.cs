﻿using ClientApp.DB_Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientApp.Pages
{
    /// <summary>
    /// Interaction logic for Services.xaml
    /// </summary>
    public partial class Services : Window
    {
        MainPage mainPage;
        Entities db = new Entities();

        public Services()
        {
            InitializeComponent();
            db = new Entities();
            db.PS.Load();
            db.Photo_services.Load();
            myDataGrid.ItemsSource = db.PS.Local.ToBindingList();
            this.Closing += MainWindow_Closing;
        }

        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            db.Dispose();
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        private void Exit(object sender, RoutedEventArgs e)
        {
            mainPage = new MainPage();
            mainPage.Show();
            this.Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            AddBox.Visibility = Visibility.Visible;
        }

        private void deleteClient_Click2(object sender, RoutedEventArgs e)
        {
            if (myDataGrid.SelectedItem != null)
            {
                ClientApp.DB_Model.P ps = myDataGrid.SelectedItem as ClientApp.DB_Model.P;
                ClientApp.DB_Model.Photo_service photo_service = new ClientApp.DB_Model.Photo_service();
                int id = ps.Photo_service_id;
                photo_service = db.Photo_services.Find(id);
                if (photo_service != null)
                {
                    db.Photo_services.Remove(photo_service);
                    db.SaveChanges();
                    DeleteBox.Visibility = Visibility.Collapsed;
                    MessageBox.Show("Видалення пройшло успішно", "Видалення клієнта", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show("Empty client");
                }
                db = new Entities();
                db.Photo_services.Load();
                db.PS.Load();
                myDataGrid.ItemsSource = db.PS.Local.ToBindingList();
            }
        }

        private void addClient_Click(object sender, RoutedEventArgs e)
        {
            ClientApp.DB_Model.P ps = myDataGrid.SelectedItem as ClientApp.DB_Model.P;
            ClientApp.DB_Model.Photo_service photo_service = new ClientApp.DB_Model.Photo_service();
            photo_service.Name_of_photo_service = txtPS.Text;
            photo_service.Price = decimal.Parse(txtPrice.Text);
            photo_service.Сategory = int.Parse(txtCategory.Text);
            {
                db.Photo_services.Add(photo_service);
                db.SaveChanges();
                AddBox.Visibility = Visibility.Collapsed;
                MessageBox.Show("Створення пройшло успішно", "Створення клієнта", MessageBoxButton.OK, MessageBoxImage.Information);
                txtPS.Text = String.Empty;
                txtPrice.Text = String.Empty;
                txtCategory.Text = String.Empty;
            }
            db = new Entities();
            db.Photo_services.Load();
            db.PS.Load();
            myDataGrid.ItemsSource = db.PS.Local.ToBindingList();
        }

        private void updateClient_Click1(object sender, RoutedEventArgs e)
        {
            if (myDataGrid.SelectedItem != null)
            {
                ClientApp.DB_Model.P ps = myDataGrid.SelectedItem as ClientApp.DB_Model.P;
                ClientApp.DB_Model.Photo_service photo_service = new ClientApp.DB_Model.Photo_service();
                int id = ps.Photo_service_id;
                photo_service = db.Photo_services.Find(id);
                photo_service.Name_of_photo_service = txtPS1.Text;
                photo_service.Price = decimal.Parse(txtPrice1.Text);
                photo_service.Сategory = int.Parse(txtCategory1.Text);
                if (photo_service != null)
                {
                    myDataGrid.Items.Refresh();
                    db.SaveChanges();
                    UpdateBox.Visibility = Visibility.Collapsed;
                    MessageBox.Show("Оновлення пройшло успішно", "Оновлення клієнта", MessageBoxButton.OK, MessageBoxImage.Information);
                    txtPS1.Text = String.Empty;
                    txtPrice1.Text = String.Empty;
                    txtCategory1.Text = String.Empty;
                }
            }
            else
            {
                MessageBox.Show("");
            }
            db = new Entities();
            db.Photo_services.Load();
            db.PS.Load();
            myDataGrid.ItemsSource = db.PS.Local.ToBindingList();
        }


        private void deleteBtn_Click(object sender, RoutedEventArgs e)
        {
            DeleteBox.Visibility = Visibility.Visible;
        }

        private void info_Click(object sender, RoutedEventArgs e)
        {
            if (myDataGrid.SelectedItem != null)
            {
                ClientApp.DB_Model.P ps = myDataGrid.SelectedItem as ClientApp.DB_Model.P;
                ClientApp.DB_Model.Photo_service photo_service = new ClientApp.DB_Model.Photo_service();
                int id = ps.Photo_service_id;
                photo_service = db.Photo_services.Find(id);
                txtPS2.Text = photo_service.Name_of_photo_service;
                txtPrice2.Text = photo_service.Price.ToString();
                txtCategory2.Text = photo_service.Сategory.ToString();
                if (photo_service != null)
                {
                    InfoBox.Visibility = Visibility.Visible;
                    txtPS2.Text = photo_service.Name_of_photo_service;
                    txtPrice2.Text = photo_service.Price.ToString();
                    txtCategory2.Text = photo_service.Сategory.ToString();
                }
            }
        }

        private void updateBtn_Click(object sender, RoutedEventArgs e)
        {
            if (myDataGrid.SelectedItem != null)
            {
                ClientApp.DB_Model.P ps = myDataGrid.SelectedItem as ClientApp.DB_Model.P;
                ClientApp.DB_Model.Photo_service photo_service = new ClientApp.DB_Model.Photo_service();
                int id = ps.Photo_service_id;
                photo_service = db.Photo_services.Find(id);
                txtPS1.Text = photo_service.Name_of_photo_service;
                txtPrice1.Text = photo_service.Price.ToString();
                txtCategory1.Text = photo_service.Сategory.ToString();
                if (photo_service != null)
                {
                    UpdateBox.Visibility = Visibility.Visible;
                    txtPS1.Text = photo_service.Name_of_photo_service;
                    txtPrice1.Text = photo_service.Price.ToString();
                    txtCategory1.Text = photo_service.Сategory.ToString();
                }
            }
        }

        private void NoButton_Click(object sender, RoutedEventArgs e)
        {
            AddBox.Visibility = Visibility.Collapsed;
        }

        private void NoButton_Click1(object sender, RoutedEventArgs e)
        {
            UpdateBox.Visibility = Visibility.Collapsed;
        }

        private void NoButton_Click2(object sender, RoutedEventArgs e)
        {
            DeleteBox.Visibility = Visibility.Collapsed;
        }

        private void infcls(object sender, RoutedEventArgs e)
        {
            InfoBox.Visibility = Visibility.Collapsed;
        }
    }
}
