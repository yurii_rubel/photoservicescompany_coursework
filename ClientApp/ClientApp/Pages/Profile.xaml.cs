﻿using ClientApp.DB_Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientApp.Pages
{
    /// <summary>
    /// Interaction logic for Profile.xaml
    /// </summary>
    public partial class Profile : Window
    {
        MainPage mainPage;
        Entities db = new Entities();
        SqlConnection con;
        SqlCommand cmd;
        SqlDataReader reader;
        static String connectionString =
            @"Data Source=DESKTOP-6RB89KD\SQLEXPRESS; Initial Catalog=PhotoServicesCompany(Coursework); Integrated Security=True;";
        
        public Profile()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
                try
                {
                    string Empid = "Select * from Employees, Position where [Employee id] = "+Emp_info.EmpId+" and [Postion id] = "+Emp_info.EmpPosition;
                    con = new SqlConnection(connectionString);
                    cmd = new SqlCommand(Empid, con);
                    con.Open();
                    reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                     One.Content = reader["First name"].ToString();
                     Two.Content = reader["Last name"].ToString();
                     Three.Content = reader["Name of position"].ToString();
                     Four.Content = reader["Address"].ToString();
                     Five.Content = reader["Phone number"].ToString();
                     Six.Content = reader["Login"].ToString();
                     Seven.Content = reader["Password"].ToString();
                    }
                    reader.Close();
                    reader.Dispose();
                    cmd.Dispose();
                    con.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        private void Exit(object sender, RoutedEventArgs e)
        {
            mainPage = new MainPage();
            mainPage.Show();
            this.Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
