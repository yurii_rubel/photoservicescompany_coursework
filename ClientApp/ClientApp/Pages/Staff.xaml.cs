﻿using ClientApp.DB_Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientApp.Pages
{
    /// <summary>
    /// Interaction logic for Staff.xaml
    /// </summary>
    public partial class Staff : Window
    {
        MainPage mainPage;
        Entities db = new Entities();

        public Staff()
        {
            InitializeComponent();
            db = new Entities();
            db.Staff1.Load();
            db.Employees.Load();
            myDataGrid.ItemsSource = db.Staff1.Local.ToBindingList();
            this.Closing += MainWindow_Closing;
        }

        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            db.Dispose();
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        private void Exit(object sender, RoutedEventArgs e)
        {
            mainPage = new MainPage();
            mainPage.Show();
            this.Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            AddBox.Visibility = Visibility.Visible;
        }

        private void deleteClient_Click2(object sender, RoutedEventArgs e)
        {
            if (myDataGrid.SelectedItem != null)
            {
                ClientApp.DB_Model.Staff1 staff1 = myDataGrid.SelectedItem as ClientApp.DB_Model.Staff1;
                ClientApp.DB_Model.Employee employee = new ClientApp.DB_Model.Employee();
                int id = staff1.Employee_id;
                employee = db.Employees.Find(id);
                if (employee != null)
                {
                    db.Employees.Remove(employee);
                    db.SaveChanges();
                    DeleteBox.Visibility = Visibility.Collapsed;
                    MessageBox.Show("Видалення пройшло успішно", "Видалення клієнта", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show("Empty client");
                }
                db = new Entities();
                db.Employees.Load();
                db.Staff1.Load();
                myDataGrid.ItemsSource = db.Staff1.Local.ToBindingList();
            }
        }

        private void addClient_Click(object sender, RoutedEventArgs e)
        {
            ClientApp.DB_Model.Staff1 staff1 = myDataGrid.SelectedItem as ClientApp.DB_Model.Staff1;
            ClientApp.DB_Model.Employee employee = new ClientApp.DB_Model.Employee();
            employee.First_name = txtFirstName.Text;
            employee.Last_name = txtLastName.Text;
            employee.Position = int.Parse(txtPosition.Text);
            employee.Salary = decimal.Parse(txtSalary.Text);
            employee.Phone_number = txtPhone.Text;
            employee.Address = txtAddress.Text;
            employee.Login = txtLog.Text;
            employee.Password = txtPas.Text;
            {
                db.Employees.Add(employee);
                db.SaveChanges();
                AddBox.Visibility = Visibility.Collapsed;
                MessageBox.Show("Створення пройшло успішно", "Створення клієнта", MessageBoxButton.OK, MessageBoxImage.Information);
                txtFirstName.Text = String.Empty;
                txtLastName.Text = String.Empty;
                txtPosition.Text = String.Empty;
                txtSalary.Text = String.Empty;
                txtPhone.Text = String.Empty;
                txtAddress.Text = String.Empty;
                txtLog.Text = String.Empty;
                txtPas.Text = String.Empty;

            }
            db = new Entities();
            db.Employees.Load();
            db.Staff1.Load();
            myDataGrid.ItemsSource = db.Staff1.Local.ToBindingList();
        }

        private void updateClient_Click1(object sender, RoutedEventArgs e)
        {
            if (myDataGrid.SelectedItem != null)
            {
                ClientApp.DB_Model.Staff1 staff1 = myDataGrid.SelectedItem as ClientApp.DB_Model.Staff1;
                ClientApp.DB_Model.Employee employee = new ClientApp.DB_Model.Employee();
                int id = staff1.Employee_id;
                employee = db.Employees.Find(id);
                employee.First_name = txtFirstName1.Text;
                employee.Last_name = txtLastName1.Text;
                employee.Position = int.Parse(txtPosition1.Text);
                employee.Salary = decimal.Parse(txtSalary1.Text);
                employee.Phone_number = txtPhone1.Text;
                employee.Address = txtAddress1.Text;
                employee.Login = txtLog1.Text;
                employee.Password = txtPas1.Text;
                if (employee != null)
                {
                    myDataGrid.Items.Refresh();
                    db.SaveChanges();
                    UpdateBox.Visibility = Visibility.Collapsed;
                    MessageBox.Show("Оновлення пройшло успішно", "Оновлення клієнта", MessageBoxButton.OK, MessageBoxImage.Information);
                    txtFirstName1.Text = String.Empty;
                    txtLastName1.Text = String.Empty;
                    txtPosition1.Text = String.Empty;
                    txtSalary1.Text = String.Empty;
                    txtPhone1.Text = String.Empty;
                    txtAddress1.Text = String.Empty;
                    txtLog1.Text = String.Empty;
                    txtPas1.Text = String.Empty;
                }
            }
            else
            {
                MessageBox.Show("");
            }
            db = new Entities();
            db.Employees.Load();
            db.Staff1.Load();
            myDataGrid.ItemsSource = db.Staff1.Local.ToBindingList();
        }


        private void deleteBtn_Click(object sender, RoutedEventArgs e)
        {
            DeleteBox.Visibility = Visibility.Visible;
        }

        private void info_Click(object sender, RoutedEventArgs e)
        {
            if (myDataGrid.SelectedItem != null)
            {
                ClientApp.DB_Model.Staff1 staff1 = myDataGrid.SelectedItem as ClientApp.DB_Model.Staff1;
                ClientApp.DB_Model.Employee employee = new ClientApp.DB_Model.Employee();
                int id = staff1.Employee_id;
                employee = db.Employees.Find(id);
                txtFirstName2.Text = employee.First_name;
                txtLastName2.Text = employee.Last_name;
                txtPosition2.Text = employee.Position.ToString();
                txtSalary2.Text = employee.Salary.ToString();
                txtPhone2.Text = employee.Phone_number;
                txtAddress2.Text = employee.Address;
                txtLog2.Text = employee.Login;
                txtPas2.Text = employee.Password;
                if (employee != null)
                {
                    InfoBox.Visibility = Visibility.Visible;
                    txtFirstName2.Text = employee.First_name;
                    txtLastName2.Text = employee.Last_name;
                    txtPosition2.Text = employee.Position.ToString();
                    txtSalary2.Text = employee.Salary.ToString();
                    txtPhone2.Text = employee.Phone_number;
                    txtAddress.Text = employee.Address;
                    txtLog2.Text = employee.Login;
                    txtPas2.Text = employee.Password;
                }
            }
        }

        private void updateBtn_Click(object sender, RoutedEventArgs e)
        {
            if (myDataGrid.SelectedItem != null)
            {
                ClientApp.DB_Model.Staff1 staff1 = myDataGrid.SelectedItem as ClientApp.DB_Model.Staff1;
                ClientApp.DB_Model.Employee employee = new ClientApp.DB_Model.Employee();
                int id = staff1.Employee_id;
                employee = db.Employees.Find(id);
                txtFirstName1.Text = employee.First_name;
                txtLastName1.Text = employee.Last_name;
                txtPosition1.Text = employee.Position.ToString();
                txtSalary1.Text = employee.Salary.ToString();
                txtPhone1.Text = employee.Phone_number;
                txtAddress1.Text = employee.Address;
                txtLog1.Text = employee.Login;
                txtPas1.Text = employee.Password;
                if (employee != null)
                {
                    UpdateBox.Visibility = Visibility.Visible;
                    txtFirstName1.Text = employee.First_name;
                    txtLastName1.Text = employee.Last_name;
                    txtPosition1.Text = employee.Position.ToString();
                    txtSalary1.Text = employee.Salary.ToString();
                    txtPhone1.Text = employee.Phone_number;
                    txtAddress1.Text = employee.Address;
                    txtLog1.Text = employee.Login;
                    txtPas1.Text = employee.Password;
                }
            }
        }

        private void NoButton_Click(object sender, RoutedEventArgs e)
        {
            AddBox.Visibility = Visibility.Collapsed;
        }

        private void NoButton_Click1(object sender, RoutedEventArgs e)
        {
            UpdateBox.Visibility = Visibility.Collapsed;
        }

        private void NoButton_Click2(object sender, RoutedEventArgs e)
        {
            DeleteBox.Visibility = Visibility.Collapsed;
        }

        private void infcls(object sender, RoutedEventArgs e)
        {
            InfoBox.Visibility = Visibility.Collapsed;
        }
    }
}
