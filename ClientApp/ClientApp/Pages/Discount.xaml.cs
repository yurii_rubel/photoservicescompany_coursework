﻿using ClientApp.DB_Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientApp.Pages
{
    /// <summary>
    /// Interaction logic for Discount.xaml
    /// </summary>
    public partial class Discount : Window
    {
        MainPage mainPage;
        Entities db = new Entities();

        public Discount()
        {
            InitializeComponent();
            db = new Entities();
            db.Discounts.Load();
            myDataGrid.ItemsSource = db.Discounts.Local.ToBindingList();
            this.Closing += MainWindow_Closing;
        }

        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            db.Dispose();
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        private void addDiscount(object sender, RoutedEventArgs e)
        {
            AddBox.Visibility = Visibility.Visible;
        }

        private void deleteClient_Click2(object sender, RoutedEventArgs e)
        {
            if (myDataGrid.SelectedItems.Count > 0)
            {
                for (int i = 0; i < myDataGrid.SelectedItems.Count; i++)
                {
                    ClientApp.DB_Model.Discount discount = myDataGrid.SelectedItems[i] as ClientApp.DB_Model.Discount;
                    if (discount != null)
                    {
                        db.Discounts.Remove(discount);
                        db.SaveChanges();
                        DeleteBox.Visibility = Visibility.Collapsed;
                        MessageBox.Show("Видалення пройшло успішно", "Видалення знижки", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
        }

        private void addClient_Click(object sender, RoutedEventArgs e)
        {
            ClientApp.DB_Model.Discount discount = new ClientApp.DB_Model.Discount();
            discount.Name_of_discount = txtName.Text;
            discount.The_amount_of_the_discount = int.Parse(txtAmount.Text);
            {
                db.Discounts.Add(discount);
                db.SaveChanges();
                AddBox.Visibility = Visibility.Collapsed;
                MessageBox.Show("Створення пройшло успішно", "Створення знижки", MessageBoxButton.OK, MessageBoxImage.Information);
                txtName.Text = String.Empty;
                txtAmount.Text = String.Empty;
            }
        }

        private void updateClient_Click1(object sender, RoutedEventArgs e)
        {
            if (myDataGrid.SelectedItems.Count > 0)
            {
                for (int i = 0; i < myDataGrid.SelectedItems.Count; i++)
                {
                    ClientApp.DB_Model.Discount discount = myDataGrid.SelectedItems[i] as ClientApp.DB_Model.Discount;
                    discount.Name_of_discount = txtName1.Text;
                    discount.The_amount_of_the_discount = int.Parse(txtAmount1.Text);
                    if (discount != null)
                    {
                        myDataGrid.Items.Refresh();
                        db.SaveChanges();
                        UpdateBox.Visibility = Visibility.Collapsed;
                        MessageBox.Show("Оновлення пройшло успішно", "Оновлення знижки", MessageBoxButton.OK, MessageBoxImage.Information);
                        txtName1.Text = String.Empty;
                        txtAmount1.Text = String.Empty;
                    }
                }
            }
        }


        private void deleteBtn_Click(object sender, RoutedEventArgs e)
        {
            DeleteBox.Visibility = Visibility.Visible;
        }

        private void info_Click(object sender, RoutedEventArgs e)
        {
            if (myDataGrid.SelectedItems.Count > 0)
            {
                for (int i = 0; i < myDataGrid.SelectedItems.Count; i++)
                {
                    ClientApp.DB_Model.Discount discount = myDataGrid.SelectedItems[i] as ClientApp.DB_Model.Discount;
                    txtName2.Text = discount.Name_of_discount;
                    txtAmount2.Text = discount.The_amount_of_the_discount.ToString();
                    if (discount != null)
                    {
                        InfoBox.Visibility = Visibility.Visible;
                        txtName2.Text = discount.Name_of_discount;
                        txtAmount2.Text = discount.The_amount_of_the_discount.ToString();
                    }
                }
            }
        }

        private void updateBtn_Click(object sender, RoutedEventArgs e)
        {
            if (myDataGrid.SelectedItems.Count > 0)
            {
                for (int i = 0; i < myDataGrid.SelectedItems.Count; i++)
                {
                    ClientApp.DB_Model.Discount discount = myDataGrid.SelectedItems[i] as ClientApp.DB_Model.Discount;
                    txtName1.Text = discount.Name_of_discount;
                    txtAmount1.Text = discount.The_amount_of_the_discount.ToString();
                    if (discount != null)
                    {
                        UpdateBox.Visibility = Visibility.Visible;
                        txtName1.Text = discount.Name_of_discount;
                        txtAmount1.Text = discount.The_amount_of_the_discount.ToString();
                    }
                }
            }
        }

        private void NoButton_Click(object sender, RoutedEventArgs e)
        {
            AddBox.Visibility = Visibility.Collapsed;
        }

        private void NoButton_Click1(object sender, RoutedEventArgs e)
        {
            UpdateBox.Visibility = Visibility.Collapsed;
        }

        private void NoButton_Click2(object sender, RoutedEventArgs e)
        {
            DeleteBox.Visibility = Visibility.Collapsed;
        }

        private void infcls(object sender, RoutedEventArgs e)
        {
            InfoBox.Visibility = Visibility.Collapsed;
        }

        private void Exit(object sender, RoutedEventArgs e)
        {
            mainPage = new MainPage();
            mainPage.Show();
            this.Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
