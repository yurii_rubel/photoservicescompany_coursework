﻿using ClientApp.DB_Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.VisualBasic;

namespace ClientApp.Pages
{
    /// <summary>
    /// Interaction logic for MainPage.xaml
    /// </summary>
    public partial class MainPage : Window
    {
        SignIn signIn;
        Profile profile;
        Client client;
        Orders orders;
        Services services;
        Discount discount;
        Staff staff;

        SqlConnection con;
        SqlCommand cmd;
        SqlDataReader reader;
        static String connectionString =
            @"Data Source=DESKTOP-6RB89KD\SQLEXPRESS; Initial Catalog=PhotoServicesCompany(Coursework); Integrated Security=True;";

        public MainPage()
        {
            InitializeComponent();
        }

        //delimitation of access rights
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (Emp_info.EmpPosition == "1")
            {
                Item1.Visibility = Visibility.Visible;
                Item2.Visibility = Visibility.Visible;
                Item3.Visibility = Visibility.Visible;
                Item4.Visibility = Visibility.Visible;
                Item5.Visibility = Visibility.Visible;
                Item6.Visibility = Visibility.Visible;
            }
            if (Emp_info.EmpPosition == "2")
            {
                Item1.Visibility = Visibility.Visible;
                Item2.Visibility = Visibility.Visible;
                Item3.Visibility = Visibility.Visible;
                Item4.Visibility = Visibility.Visible;
                Item5.Visibility = Visibility.Visible;
                Item6.Visibility = Visibility.Visible;
            }
            if (Emp_info.EmpPosition == "3")
            {
                Item1.Visibility = Visibility.Visible;
                Item2.Visibility = Visibility.Visible;
                Item3.Visibility = Visibility.Visible;
                Item4.Visibility = Visibility.Visible;
                Item5.Visibility = Visibility.Hidden;
                Item6.Visibility = Visibility.Hidden;
            }
            if (Emp_info.EmpPosition == "5")
            {
                Item1.Visibility = Visibility.Visible;
                Item2.Visibility = Visibility.Visible;
                Item3.Visibility = Visibility.Visible;
                Item4.Visibility = Visibility.Visible;
                Item5.Visibility = Visibility.Visible;
                Item6.Visibility = Visibility.Hidden;
            }
        }

        private void ListViewItem_MouseEnter(object sender, MouseEventArgs e)
        {
            // Set tooltip visibility

            if (Tg_Btn.IsChecked == true)
            {
                tt_home.Visibility = Visibility.Collapsed;
                tt_contacts.Visibility = Visibility.Collapsed;
                tt_messages.Visibility = Visibility.Collapsed;
                tt_maps.Visibility = Visibility.Collapsed;
                tt_settings.Visibility = Visibility.Collapsed;
                tt_signout.Visibility = Visibility.Collapsed;
            }
            else
            {
                tt_home.Visibility = Visibility.Visible;
                tt_contacts.Visibility = Visibility.Visible;
                tt_messages.Visibility = Visibility.Visible;
                tt_maps.Visibility = Visibility.Visible;
                tt_settings.Visibility = Visibility.Visible;
                tt_signout.Visibility = Visibility.Visible;
            }
        }

        private void Tg_Btn_Unchecked(object sender, RoutedEventArgs e)
        {
            img_bg.Opacity = 1;
        }

        private void Tg_Btn_Checked(object sender, RoutedEventArgs e)
        {
            img_bg.Opacity = 0.3;
        }

        private void BG_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Tg_Btn.IsChecked = false;
        }
        
        private void Exit(object sender, RoutedEventArgs e)
        {
            signIn = new SignIn();
            signIn.Show();
            this.Close();
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        private void Profile(object sender, RoutedEventArgs e)
        {
            InputBox.Visibility = Visibility.Visible;
        }

        private void YesButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Emp_info.EmpLogin == txtUsername.Text)
                {
                    con = new SqlConnection(connectionString);
                    con.Open();
                    cmd = new SqlCommand("Select * from Employees where Login=@Login and Password=@Password", con);
                    cmd.Parameters.AddWithValue("@Login", txtUsername.Text.ToString());
                    cmd.Parameters.AddWithValue("@Password", txtPassword.Password.ToString());
                    reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        if (reader["Password"].ToString().Equals(txtPassword.Password.ToString(), StringComparison.InvariantCulture))
                        {
                            Emp_info.EmpLogin = txtUsername.Text.ToString();
                            Emp_info.EmpId = reader["Employee id"].ToString();
                            Emp_info.EmpPosition = reader["Position"].ToString();
                        }
                        profile = new Profile();
                        profile.Show();
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Логін або пароль введені невірно", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    reader.Close();
                    reader.Dispose();
                    cmd.Dispose();
                    con.Close();
                }
                else
                {
                    MessageBox.Show("Логін або пароль введені невірно", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void NoButton_Click(object sender, RoutedEventArgs e)
        {
            InputBox.Visibility = Visibility.Collapsed;
            txtUsername.Text = String.Empty;
            txtPassword.Password = String.Empty;
        }

        private void Client(object sender, RoutedEventArgs e)
        {
            client = new Client();
            client.Show();
            this.Close();
        }

        private void Orders(object sender, RoutedEventArgs e)
        {
            orders = new Orders();
            orders.Show();
            this.Close();
        }

        private void Services(object sender, RoutedEventArgs e)
        {
            services = new Services();
            services.Show();
            this.Close();
        }

        private void Discount(object sender, RoutedEventArgs e)
        {
            discount = new Discount();
            discount.Show();
            this.Close();
        }

        private void Staff(object sender, RoutedEventArgs e)
        {
            staff = new Staff();
            staff.Show();
            this.Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
