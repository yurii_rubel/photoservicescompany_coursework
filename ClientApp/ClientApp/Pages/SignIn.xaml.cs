﻿using ClientApp.DB_Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientApp.Pages
{
    /// <summary>
    /// Interaction logic for SignIn.xaml
    /// </summary>
    public partial class SignIn : Window
    {
        MainPage mainPage;
        MainWindow mainWindow;

        SqlConnection con;
        SqlCommand cmd;
        SqlDataReader reader;
        static String connectionString = 
            @"Data Source=DESKTOP-6RB89KD\SQLEXPRESS; Initial Catalog=PhotoServicesCompany(Coursework); Integrated Security=True;";

        public static string position;
        public SignIn()
        {
            InitializeComponent();
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        private void BackToMain(object sender, RoutedEventArgs e)
        {
            mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }

        //delimitation of access rights
        private void Signin(object sender, RoutedEventArgs e)
        {
            try
            {
                con = new SqlConnection(connectionString);
                con.Open();
                cmd = new SqlCommand("Select * from Employees where Login=@Login and Password=@Password", con);
                cmd.Parameters.AddWithValue("@Login", txtUsername.Text.ToString());
                cmd.Parameters.AddWithValue("@Password", txtPassword.Password.ToString());
                reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    if (reader["Password"].ToString().Equals(txtPassword.Password.ToString(), StringComparison.InvariantCulture))
                    {
                        Emp_info.EmpLogin = txtUsername.Text.ToString();
                        Emp_info.EmpId = reader["Employee id"].ToString();
                        Emp_info.EmpPosition = reader["Position"].ToString();
                    }
                    mainPage = new MainPage();
                    mainPage.Show();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Логін або пароль введені невірно", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                reader.Close();
                reader.Dispose();
                cmd.Dispose();
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void CloseBtn_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}