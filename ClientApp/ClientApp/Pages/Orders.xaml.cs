﻿using ClientApp.DB_Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientApp.Pages
{
    /// <summary>
    /// Interaction logic for Orders.xaml
    /// </summary>
    public partial class Orders : Window
    {
        MainPage mainPage;
        AddOrder addorder;
        InfOrder inforder;
        DoOrder doorder;
        UpdateOrder updateorder;
        DeleteOrder deleteorder;
        Entities db = new Entities();
 
        public Orders()
        {
            InitializeComponent();
            db = new Entities();
            db.PhotoServs.Load();
            db.Photo_services.Load();
            myDataGrid.ItemsSource = db.PhotoServs.Local.ToBindingList();
            this.Closing += MainWindow_Closing;
        }

        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            db.Dispose();
        }

        private void AddOrder1(object sender, RoutedEventArgs e)
        {
            addorder = new AddOrder();
            addorder.Show();
            this.Close();
        }

        private void Info1(object sender, RoutedEventArgs e)
        {
            if (myDataGrid.SelectedItem != null)
            {
                ClientApp.DB_Model.PhotoServ ph_serv = myDataGrid.SelectedItem as ClientApp.DB_Model.PhotoServ;
                ClientApp.DB_Model.Order OrDer = new ClientApp.DB_Model.Order();
                int id = ph_serv.Order_id;
                OrDer = db.Orders.Find(id);
                Order_info.OrderID = OrDer.Order_id.ToString();
            }
            inforder = new InfOrder();
            inforder.Show();
            this.Close();
        }

        private void Do1(object sender, RoutedEventArgs e)
        {
            if (myDataGrid.SelectedItem != null)
            {
                ClientApp.DB_Model.PhotoServ ph_serv = myDataGrid.SelectedItem as ClientApp.DB_Model.PhotoServ;
                ClientApp.DB_Model.Order OrDer = new ClientApp.DB_Model.Order();
                int id = ph_serv.Order_id;
                OrDer = db.Orders.Find(id);
                Order_info.OrderID = OrDer.Order_id.ToString();
            }
            doorder = new DoOrder();
            doorder.Show();
            this.Close();
        }

        private void Update1(object sender, RoutedEventArgs e)
        {
            if (myDataGrid.SelectedItem != null)
            {
                ClientApp.DB_Model.PhotoServ ph_serv = myDataGrid.SelectedItem as ClientApp.DB_Model.PhotoServ;
                ClientApp.DB_Model.Order OrDer = new ClientApp.DB_Model.Order();
                int id = ph_serv.Order_id;
                OrDer = db.Orders.Find(id);
                Order_info.OrderID = OrDer.Order_id.ToString();
            }
            updateorder = new UpdateOrder();
            updateorder.Show();
            this.Close();
        }

        private void Delete1(object sender, RoutedEventArgs e)
        {
            if (myDataGrid.SelectedItem != null)
            {
                ClientApp.DB_Model.PhotoServ ph_serv = myDataGrid.SelectedItem as ClientApp.DB_Model.PhotoServ;
                ClientApp.DB_Model.Order OrDer = new ClientApp.DB_Model.Order();
                int id = ph_serv.Order_id;
                OrDer = db.Orders.Find(id);
                Order_info.OrderID = OrDer.Order_id.ToString();
            }
            deleteorder = new DeleteOrder();
            deleteorder.Show();
            this.Close();
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        private void Exit(object sender, RoutedEventArgs e)
        {
            mainPage = new MainPage();
            mainPage.Show();
            this.Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
