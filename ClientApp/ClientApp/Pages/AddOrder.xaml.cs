﻿using ClientApp.DB_Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientApp.Pages
{
    /// <summary>
    /// Interaction logic for DeleteOrder.xaml
    /// </summary>
    public partial class AddOrder : Window
    {
        MainPage mainpage;
        Orders orders;
        string clientID;
        string paymentTypeID;
        Entities db = new Entities();
        SqlConnection con;
        SqlCommand cmd;
        SqlDataReader reader;
        static String connectionString =
            @"Data Source=DESKTOP-6RB89KD\SQLEXPRESS; Initial Catalog=PhotoServicesCompany(Coursework); Integrated Security=True;";

        public AddOrder()
        {
            InitializeComponent();
            db = new Entities();
            db.Photo_services.Load();
            db.Discounts.Load();
            this.Closing += MainWindow_Closing;
        }

        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            db.Dispose();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //random
            Random rnd = new Random();
            int randomnum = rnd.Next(10000, 99999);
            txtnumorder.Text = randomnum.ToString();

            //time(now)
            txtdate1.Text = DateTime.Now.ToString();

            //
            try
            {
                string Empid = "Select * from [Order state] where [Order state id] = 1";
                con = new SqlConnection(connectionString);
                cmd = new SqlCommand(Empid, con);
                con.Open();
                reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    txtOrderState.Text = reader["Name of orders state"].ToString();
                }
                reader.Close();
                reader.Dispose();
                cmd.Dispose();
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            //show employee
            try
            {
                string Empid = "Select * from Employees, Position where [Employee id] = " + Emp_info.EmpId + " and [Postion id] = " + Emp_info.EmpPosition;
                con = new SqlConnection(connectionString);
                cmd = new SqlCommand(Empid, con);
                con.Open();
                reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    txtemployee.Text = reader["First name"].ToString();
                    txtemployee2.Text = reader["Last name"].ToString();
                }
                reader.Close();
                reader.Dispose();
                cmd.Dispose();
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            //
            client_load();
            photoservices_load();
            payments_load();
        }

        public void client_load()
        {
            con.Open();
            string q = "Select c.[Client id], c.[First Name], c.[Last Name], c.[Phone number], c.Email, c.Address, d.[Name of discount], d.[The amount of the discount] From Clients AS c JOIN Discount AS d ON c.Discount = d.[Discount id]";
            SqlCommand cmd = new SqlCommand(q, con);
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                Client.Items.Add(dr["Last Name"].ToString());
            }
            reader.Close();
            reader.Dispose();
            cmd.Dispose();
            con.Close();
        }

        public void photoservices_load()
        {
            con.Open();
            string q = "Select * from [Photo services]";
            SqlCommand cmd = new SqlCommand(q, con);
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                PhotoServicesCombobox.Items.Add(dr["Name of photo service"].ToString());
            }
            reader.Close();
            reader.Dispose();
            cmd.Dispose();
            con.Close();
        }

        public void payments_load()
        {
            con.Open();
            string q = "Select * from [Payment type]";
            SqlCommand cmd = new SqlCommand(q, con);
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                Paymentcombobox.Items.Add(dr["Name of payment type"].ToString());
            }
            reader.Close();
            reader.Dispose();
            cmd.Dispose();
            con.Close();
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        private void Back(object sender, RoutedEventArgs e)
        {
            orders = new Orders();
            orders.Show();
            this.Close();
        }

        private void Exit(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void ExitMain(object sender, RoutedEventArgs e)
        {
            mainpage = new MainPage();
            mainpage.Show();
            this.Close();
        }

        private void Client_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                string Empid = "Select * from Clients JOIN Discount ON Clients.Discount = Discount.[Discount id] where Clients.[Last Name] = '" + Client.SelectedItem.ToString() + "' ";
                con = new SqlConnection(connectionString);
                cmd = new SqlCommand(Empid, con);
                con.Open();
                reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    this.clientID = reader["Client id"].ToString();
                    txtFName.Text = reader["First Name"].ToString();
                    txtLName.Text = reader["Last Name"].ToString();
                    txtPhone.Text = reader["Phone number"].ToString();
                    txtEmail.Text = reader["Email"].ToString();
                    txtAddress.Text = reader["Address"].ToString();
                    txtDiscount.Text = reader["The amount of the discount"].ToString();
                }
                reader.Close();
                reader.Dispose();
                cmd.Dispose();
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void AddCount(object sender, RoutedEventArgs e)
        {
            List<ClientApp.DB_Model.Photo_service> ps = new List<ClientApp.DB_Model.Photo_service>();
            ps = db.Photo_services.ToList();
            for (int i = 0; i < ps.Count; i++)
            {
                if (ps[i].Name_of_photo_service == PhotoServicesCombobox.SelectedItem.ToString())
                {
                    myDataGrid.Items.Add(ps[i]);
                }
            }
        }

        private void SubTotal(object sender, RoutedEventArgs e)
        {
            try
            {
                decimal sum1 = 0;
                decimal discount1 = 0; 

                for (int i = 0; i < myDataGrid.Items.Count; i++)
                {
                    Photo_service updatedPhoto_service = (Photo_service)myDataGrid.Items[i];
                    sum1 += updatedPhoto_service.Price;
                }

                if(txtDiscount.Text != null)
                {
                    if(Int32.Parse(txtDiscount.Text)>0 && Int32.Parse(txtDiscount.Text)<100)
                    {
                        discount1 = sum1 / 100 * Int32.Parse(txtDiscount.Text);
                        sum1 = sum1 - discount1;
                    }
                }

                txtAmount.Text = sum1.ToString();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }

        private void Paymentcombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                string Empid = "Select * from [Payment type] where [Payment type].[Name of payment type] ='" + Paymentcombobox.SelectedItem.ToString() + "' ";
                con = new SqlConnection(connectionString);
                cmd = new SqlCommand(Empid, con);
                con.Open();
                reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    this.paymentTypeID = reader["Payment type id"].ToString();
                    txtpayment.Text = reader["Name of payment type"].ToString();
                }
                reader.Close();
                reader.Dispose();
                cmd.Dispose();
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void AddClientOrder(object sender, RoutedEventArgs e)
        {
            ClientApp.DB_Model.Order order = new ClientApp.DB_Model.Order();
            ClientApp.DB_Model.Selected_service selected_Service = new ClientApp.DB_Model.Selected_service();
            ClientApp.DB_Model.Payment payment = new ClientApp.DB_Model.Payment();
            int ps_id = new int();
            decimal price_ps = new decimal();
            List<ClientApp.DB_Model.Photo_service> ps = new List<ClientApp.DB_Model.Photo_service>();
            ps = db.Photo_services.ToList();
            for (int i = 0; i < ps.Count; i++)
            {
                if (ps[i].Name_of_photo_service == PhotoServicesCombobox.SelectedItem.ToString())
                {
                    ps_id = ps[i].Photo_service_id;
                    price_ps = ps[i].Price;
                }
            }
            order.Order_number = int.Parse(txtnumorder.Text);
            order.Employee_id = int.Parse(Emp_info.EmpId);
            order.Client_id = int.Parse(this.clientID);
            order.Order_date = DateTime.Now;
            order.Execution_date = DateTime.Parse(txtdate2.Text);
            order.Amount_order = Decimal.Parse(txtAmount.Text);
            order.Order_state = 1;
            selected_Service.Order_id = order.Order_id;
            selected_Service.Photo_service_id = ps_id;
            selected_Service.Price_of_service = price_ps;
            selected_Service.Quantity_service = myDataGrid.Items.Count.ToString();
            payment.Order = order.Order_id;
            payment.Payment_type = int.Parse(paymentTypeID);
            payment.Amount = Decimal.Parse(txtAmount.Text);
            {
                db.Orders.Add(order);
                db.Selected_services.Add(selected_Service);
                db.Payments.Add(payment);
                db.SaveChanges();
                MessageBox.Show("Оформлення замовлення пройшло успішно", "Оформлення замовлення", MessageBoxButton.OK, MessageBoxImage.Information);
                orders = new Orders();
                orders.Show();
                this.Close();
            }
        }
    }
}
