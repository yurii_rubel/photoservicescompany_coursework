﻿using ClientApp.DB_Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientApp.Pages
{
    /// <summary>
    /// Interaction logic for AddOrder.xaml
    /// </summary>
    public partial class InfOrder : Window
    {
        MainPage mainpage;
        Orders orders;
        Entities db = new Entities();
        SqlConnection con;
        SqlCommand cmd;
        SqlDataReader reader;
        static String connectionString =
            @"Data Source=DESKTOP-6RB89KD\SQLEXPRESS; Initial Catalog=PhotoServicesCompany(Coursework); Integrated Security=True;";

        public InfOrder()
        {
            InitializeComponent();
            db = new Entities();
            this.Closing += MainWindow_Closing;
        }

        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            db.Dispose();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                string Empid = "SELECT o.[Order number], o.[Order date], o.[Execution date], o.[Amount order]," +
                    "os.[Name of orders state], e.[First name], e.[Last name], pt.[Name of payment type]," +
                    "c.[First name] as F, c.[Last name] as L, c.[Phone number], c.[Email], c.[Address], d.[The amount of the discount]" +
                    "FROM Orders AS o " +
                    "JOIN [Order state] as os ON o.[Order state] = os.[Order state id]" +
                    "JOIN Employees as e ON o.[Employee id] = e.[Employee id] " +
                    "JOIN Payments as p ON o.[Order id] = p.[Order] " +
                    "JOIN [Payment type] as pt ON p.[Payment type] = pt.[Payment type id] " +
                    "JOIN Clients as c ON o.[Client id] = c.[Client id]" +
                    "JOIN Discount as d ON c.[Discount] = d.[Discount id] " +
                    "WHERE [Order id] =" + Order_info.OrderID;
                con = new SqlConnection(connectionString);
                cmd = new SqlCommand(Empid, con);
                con.Open();
                reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    txtnumorder.Text = reader["Order number"].ToString();
                    txtdate1.Text = reader["Order date"].ToString();
                    txtdate2.Text = reader["Execution date"].ToString();
                    txtAmount.Text = reader["Amount order"].ToString();
                    txtOrderState.Text = reader["Name of orders state"].ToString();
                    txtemployee.Text = reader["First name"].ToString();
                    txtemployee2.Text = reader["Last name"].ToString();
                    txtpayment.Text = reader["Name of payment type"].ToString();
                    txtFName.Text = reader["F"].ToString();
                    txtLName.Text = reader["L"].ToString();
                    txtPhone.Text = reader["Phone number"].ToString();
                    txtEmail.Text = reader["Email"].ToString();
                    txtDiscount.Text = reader["The amount of the discount"].ToString();
                    txtAddress.Text = reader["Address"].ToString();
                }
                reader.Close();
                reader.Dispose();
                cmd.Dispose();
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        private void Back(object sender, RoutedEventArgs e)
        {
            orders = new Orders();
            orders.Show();
            this.Close();
        }

        private void Exit(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void ExitMain(object sender, RoutedEventArgs e)
        {
            mainpage = new MainPage();
            mainpage.Show();
            this.Close();
        }
    }
}
