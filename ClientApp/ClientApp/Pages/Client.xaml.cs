﻿using ClientApp.DB_Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientApp.Pages
{
    /// <summary>
    /// Interaction logic for Client.xaml
    /// </summary>
    public partial class Client : Window
    {
        MainPage mainPage;
        Entities db = new Entities();
      
        public Client()
        {
            InitializeComponent();
            db = new Entities();
            db.Clients.Load();
            db.View_1.Load();
            myDataGrid.ItemsSource = db.View_1.Local.ToBindingList();
            this.Closing += MainWindow_Closing;
        }

        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            db.Dispose();
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        private void Exit(object sender, RoutedEventArgs e)
        {
            mainPage = new MainPage();
            mainPage.Show();
            this.Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Search_Click(object sender, RoutedEventArgs e)
        {
           
        }

        private void addClient(object sender, RoutedEventArgs e)
        {
            AddBox.Visibility = Visibility.Visible;
        }

        private void deleteClient_Click2(object sender, RoutedEventArgs e)
        {
            if (myDataGrid.SelectedItem != null)
            {
                ClientApp.DB_Model.View_1 view_1 = myDataGrid.SelectedItem as ClientApp.DB_Model.View_1;
                ClientApp.DB_Model.Client client = new ClientApp.DB_Model.Client();
                int id = view_1.Client_id;
                client = db.Clients.Find(id);
                if (client != null)
                {
                    db.Clients.Remove(client);
                    db.SaveChanges();
                    DeleteBox.Visibility = Visibility.Collapsed;
                    MessageBox.Show("Видалення пройшло успішно", "Видалення клієнта", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show("Empty client");
                }
                db = new Entities();
                db.Clients.Load();
                db.View_1.Load();
                myDataGrid.ItemsSource = db.View_1.Local.ToBindingList();
            }
        }

        private void addClient_Click(object sender, RoutedEventArgs e)
        {         
            ClientApp.DB_Model.Client client = new ClientApp.DB_Model.Client();
            ClientApp.DB_Model.View_1 view_1 = myDataGrid.SelectedItem as ClientApp.DB_Model.View_1;           
            client.First_name = txtFirstName.Text;
            client.Last_name = txtLastName.Text;
            client.Phone_number = txtPhoneNumber.Text;
            client.Email = txtEmail.Text;
            client.Address = txtAddress.Text;
            client.Discount = int.Parse(txtDiscount.Text);          
            {                              
                db.Clients.Add(client);             
                db.SaveChanges();
                AddBox.Visibility = Visibility.Collapsed;
                MessageBox.Show("Створення пройшло успішно", "Створення клієнта", MessageBoxButton.OK, MessageBoxImage.Information);
                txtFirstName.Text = String.Empty;
                txtLastName.Text = String.Empty;
                txtPhoneNumber.Text = String.Empty;
                txtEmail.Text = String.Empty;
                txtAddress.Text = String.Empty;
                txtDiscount.Text = String.Empty;

            }
            db = new Entities();
            db.Clients.Load();
            db.View_1.Load();
            myDataGrid.ItemsSource = db.View_1.Local.ToBindingList();
        }
            
        private void updateClient_Click1(object sender, RoutedEventArgs e)
        {
            if (myDataGrid.SelectedItem != null)
            {
                ClientApp.DB_Model.View_1 view_1 = myDataGrid.SelectedItem as ClientApp.DB_Model.View_1;
                ClientApp.DB_Model.Client client = new ClientApp.DB_Model.Client();
                int id = view_1.Client_id;
                client = db.Clients.Find(id);
                client.First_name = txtFirstName1.Text.Trim();
                client.Last_name = txtLastName1.Text.Trim();
                client.Phone_number = txtPhoneNumber1.Text;
                client.Email = txtEmail1.Text.Trim();
                client.Address = txtAddress1.Text.Trim();              
                client.Discount = int.Parse(txtDiscount1.Text);
                if (client != null)
                {
                    myDataGrid.Items.Refresh();
                    db.SaveChanges();
                    UpdateBox.Visibility = Visibility.Collapsed;
                    MessageBox.Show("Оновлення пройшло успішно", "Оновлення клієнта", MessageBoxButton.OK, MessageBoxImage.Information);
                    txtFirstName1.Text = String.Empty;
                    txtLastName1.Text = String.Empty;
                    txtPhoneNumber1.Text = String.Empty;
                    txtEmail1.Text = String.Empty;
                    txtAddress1.Text = String.Empty;                    
                    txtDiscount1.Text = String.Empty;
                }
            }
            else
            {
                MessageBox.Show("");
            }
                db = new Entities();
                db.Clients.Load();
                db.View_1.Load();
                myDataGrid.ItemsSource = db.View_1.Local.ToBindingList();
        }
        

        private void deleteBtn_Click(object sender, RoutedEventArgs e)
        {
            DeleteBox.Visibility = Visibility.Visible;
        }

        private void info_Click(object sender, RoutedEventArgs e)
        {
            if(myDataGrid.SelectedItem != null)
            {
                ClientApp.DB_Model.View_1 view_1 = myDataGrid.SelectedItem as ClientApp.DB_Model.View_1;
                ClientApp.DB_Model.Client client = new ClientApp.DB_Model.Client();
                int id = view_1.Client_id;
                client = db.Clients.Find(id);
                txtFirstName2.Text = client.First_name;
                txtLastName2.Text = client.Last_name;
                txtPhoneNumber2.Text = client.Phone_number.ToString();
                txtEmail2.Text = client.Email;
                txtAddress2.Text = client.Address;
                txtDiscount1.Text = client.Discount.ToString();
                if (client != null)
                {
                    InfoBox.Visibility = Visibility.Visible;
                    txtFirstName2.Text = client.First_name;
                    txtLastName2.Text = client.Last_name;
                    txtPhoneNumber2.Text = client.Phone_number.ToString();
                    txtEmail2.Text = client.Email;
                    txtAddress2.Text = client.Address;
                    txtDiscount2.Text = client.Discount.ToString();
                }
            }
        }

        private void updateBtn_Click(object sender, RoutedEventArgs e)
        {
            if (myDataGrid.SelectedItem != null)
            {
                ClientApp.DB_Model.View_1 view_1 = myDataGrid.SelectedItem as ClientApp.DB_Model.View_1;
                ClientApp.DB_Model.Client client = new ClientApp.DB_Model.Client();
                int id = view_1.Client_id;
                client = db.Clients.Find(id);
                txtFirstName1.Text = client.First_name;
                txtLastName1.Text = client.Last_name;
                txtPhoneNumber1.Text = client.Phone_number.ToString();
                txtEmail1.Text = client.Email;
                txtAddress1.Text = client.Address;
                txtDiscount1.Text = client.Discount.ToString();
                if (client != null)
                {
                    UpdateBox.Visibility = Visibility.Visible;
                    txtFirstName1.Text = client.First_name;
                    txtLastName1.Text = client.Last_name;
                    txtPhoneNumber1.Text = client.Phone_number.ToString();
                    txtEmail1.Text = client.Email;
                    txtAddress1.Text = client.Address;
                    txtDiscount1.Text = client.Discount.ToString();
                }
            }         
        }

        private void NoButton_Click(object sender, RoutedEventArgs e)
        {
            AddBox.Visibility = Visibility.Collapsed;
        }

        private void NoButton_Click1(object sender, RoutedEventArgs e)
        {
            UpdateBox.Visibility = Visibility.Collapsed;
        }

        private void NoButton_Click2(object sender, RoutedEventArgs e)
        {
            DeleteBox.Visibility = Visibility.Collapsed;
        }

        private void infcls(object sender, RoutedEventArgs e)
        {
            InfoBox.Visibility = Visibility.Collapsed;
        }
    }
}
